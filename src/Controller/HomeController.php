<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Repository\PostRepository;
use App\Repository\SubcategoryRepository;
use App\Service\FavoriteManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    private $favoriteManager;
    private $session;

    public function __construct(FavoriteManager $favoriteManager, SessionInterface $session)
    {
        $this->favoriteManager = $favoriteManager;
        $this->session = $session;
    }


    /**
     * @Route("/", name="home")
     * @param CategoryRepository $categoryRepository
     * @param SubcategoryRepository $subcategoryRepository
     * @param PostRepository $postRepository
     * @return Response
     */
    public function index(CategoryRepository $categoryRepository, SubcategoryRepository $subcategoryRepository, PostRepository $postRepository) : Response
    {
        return $this->render('home/index.html.twig', [
            'categories' => $categoryRepository->findAll(),
            'subcategories' => $subcategoryRepository->findAll(),
            'posts' => $postRepository->findAll()
            ]);
    }

    /**
     * @Route("/search", name="home_search", methods={"POST"})
     * @param Request $request
     * @param PostRepository $postRepository
     * @param CategoryRepository $categoryRepository
     * @return Response
     */
    public function search(Request $request, PostRepository $postRepository, CategoryRepository $categoryRepository) : Response
    {

        $text = $request->get('search');
        $posts = $postRepository->findBySearch($text);
        return $this->render('post/index.html.twig', [
            'posts' => $posts,
            'favorites' => $this->favoriteManager->getFavoritePosts(),
            'categories' => $categoryRepository->findAll(),
            'name' => $_POST['search']
        ]);
    }


    /**
     * @Route("/currency", name="home_currency")
     * @param Request $request
     * @return RedirectResponse
     */
    public function currency(Request $request) : RedirectResponse
    {
        $referer = $request->headers->get('referer');
        $currency = $request->get('currency');

        if($currency){
            $this->session->set('currency', $currency);
            return new RedirectResponse($referer);
        } else {
            return new RedirectResponse($referer);
        }

    }


}
