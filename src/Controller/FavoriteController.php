<?php

namespace App\Controller;

use App\Entity\Favorite;
use App\Entity\Post;
use App\Entity\User;
use App\Repository\FavoriteRepository;
use App\Repository\PostRepository;
use App\Service\FavoriteManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/favorite")
 */
class FavoriteController extends AbstractController
{

    private $favoriteManager;

    public function __construct(FavoriteManager $favoriteManager)
    {
        $this->favoriteManager = $favoriteManager;
    }

    /**
     * @Route("/", name="favorite_index", methods={"GET"})
     * @param FavoriteRepository $favoriteRepository
     * @return Response
     */
    public function index(FavoriteRepository $favoriteRepository): Response
    {

        $favoritePosts = $this->favoriteManager->getFavoritePosts();

        $favorites = null;
        // admin is inMemory, so it does not have favorites
        if($this->getUser() instanceof User) {
            $favorites = $favoriteRepository->findBy(['user' => $this->getUser()]);
        }

        return $this->render('favorite/index.html.twig', [
            'posts' => $favoritePosts,
            'favorites' => $favorites
        ]);
    }

    /**
     * @Route("/new/{id}", name="favorite_new", methods={"GET"})
     * @param PostRepository $postRepository
     * @param FavoriteRepository $favoriteRepository
     * @param int $id
     * @return Response
     */
    public function new(PostRepository $postRepository, FavoriteRepository $favoriteRepository, string $id) : Response
    {

        $post = $postRepository->find($id);

        // check if post exists && if favorite was not added before
        if ($post) {
            if (!$favoriteRepository->findBy(['post' => $post,'user' => $this->getUser()])) {

                $entityManager = $this->getDoctrine()->getManager();
                $favorite = new Favorite();
                $favorite->setUser($this->getUser());
                $favorite->setPost($post);
                $favorite->setCreatedAt(time());
                $entityManager->persist($favorite);
                $entityManager->flush();

                return $this->redirectToRoute('post_index');
            } else {
                $this->addFlash('error', 'This post is already added in your favorites section');
                return $this->redirectToRoute('post_index');
            }
        } else {
            $this->addFlash('error', 'This post was not found');
            return $this->redirectToRoute('post_index');
        }

    }


    /**
     * @Route("/{id}", name="favorite_delete", methods={"GET"})
     * @param PostRepository $postRepository
     * @param FavoriteRepository $favoriteRepository
     * @param string $id
     * @return Response
     */
    public function delete(PostRepository $postRepository, FavoriteRepository $favoriteRepository, string $id) : Response
    {
        $post = $postRepository->find($id);
        if($post){

            $favorite = $favoriteRepository->findOneBy(['post' => $post,'user' => $this->getUser()]);
            if ($favorite && $favorite->getUser()->getId() == $this->getUser()->getId()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($favorite);
                $entityManager->flush();
            }
        }


        return $this->redirectToRoute('post_index');
    }


}