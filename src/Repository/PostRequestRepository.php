<?php

namespace App\Repository;

use App\Entity\PostRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PostRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostRequest[]    findAll()
 * @method PostRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRequestRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PostRequest::class);
    }


}
