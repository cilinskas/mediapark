<?php

namespace App\Tests;


use App\Entity\User;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserTest extends WebTestCase
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    protected static $application;

    public function setUp()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        $this->em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $schemaTool = new SchemaTool($this->em);
        $metadata = $this->em->getMetadataFactory()->getAllMetadata();

        $schemaTool->dropSchema($metadata);
        $schemaTool->createSchema($metadata);

        /** @var Client $client */
        $client = static::createClient();
        $this->em = $client->getContainer()->get('doctrine.orm.entity_manager');

        $loader = new Loader();
        $loader->loadFromDirectory('src/DataFixtures/Tests');

        $purger = new ORMPurger();
        $executor = new ORMExecutor($this->em, $purger);
        $executor->execute($loader->getFixtures(), true);

    }


    public function getEmailAndPasswordData()
    {
        $out['test1'] = ['test@testing.com','password','test@testing.com'];
        return $out;
    }


    /**
     * @dataProvider getEmailAndPasswordData
     */
    public function testAddUser($email,$password, $expected)
    {

        $user = new User();
        $user->setEmail($email);
        $user->setPassword($password);

        $this->em->persist($user);
        $this->em->flush();

        $user = $this->em->getRepository('App\Entity\User')->findOneBy(['email' => 'test@testing.com']);

        $this->assertEquals($expected, $user->getEmail());


        $this->em->remove($user);
        $this->em->flush();

    }


    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();
        $this->em->close();
    }

}