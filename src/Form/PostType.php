<?php

namespace App\Form;

use App\Entity\Currency;
use App\Entity\Post;
use App\Repository\CategoryRepository;
use App\Repository\SubcategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolver;



class PostType extends AbstractType
{

    private $categoryRepository;
    private $subcategoryRepository;

    public function __construct(CategoryRepository $categoryRepository, SubcategoryRepository $subcategoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->subcategoryRepository = $subcategoryRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        // Build static form fields
        $builder
            ->add('title')
            ->add('description')
            ->add('price')
            ->add('currency', EntityType::class, [
                'label' => 'Select Currency',
                'required' => true,
                'class' => Currency::class
            ])
            ->add('deliveryTimeFrom')
            ->add('deliveryTimeTo')
            ->add('deliveryTimeFormat', ChoiceType::class, [
                'label' => "Select Delivery Time Format",
                'required' => true,
                'multiple' => false,
                'choices' => [
                    'Days' => 'Days',
                    'Hours' => 'Hours',
                    'Minutes' => 'Minutes'
                ]
            ])
            ->add('postImages', CollectionType::class, [
                'entry_type' => PostImageType::class,
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'label'=>false,
            ])
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
