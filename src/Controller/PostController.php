<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\User;
use App\Form\PostType;
use App\Repository\CategoryRepository;
use App\Repository\FavoriteRepository;
use App\Repository\PostImageRepository;
use App\Repository\PostRepository;
use App\Repository\ReviewRepository;
use App\Repository\SubcategoryRepository;
use App\Service\PostManager;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/post")
 */
class PostController extends AbstractController
{

    private $postManager;

    public function __construct(PostManager $postManager)
    {
        $this->postManager = $postManager;
    }

    /**
     * @Route("/all", name="post_index", methods={"GET"})
     * @param PostRepository $postRepository
     * @param FavoriteRepository $favoriteRepository
     * @param CategoryRepository $categoryRepository
     * @return Response
     */
    public function index(PostRepository $postRepository, FavoriteRepository $favoriteRepository,
                          CategoryRepository $categoryRepository): Response
    {
        $favorites = null;
        // admin is inMemory, so it does not have favorites
        if ($this->getUser() instanceof User) {
            $favorites = $favoriteRepository->findBy(['user' => $this->getUser()]);
        }

        return $this->render('post/index.html.twig', [
            'posts' => $postRepository->findAll(),
            'favorites' => $favorites,
            'categories' => $categoryRepository->findAll(),
            'name' => 'All'
        ]);

    }


    /**
     * @Route("/manage", name="post_manage", methods={"GET"})
     */
    public function manage(): Response
    {

        $user = $this->getUser();

        $userPosts = $user->getPosts();

        return $this->render('/post/manage.html.twig', [
            'posts' => $userPosts
        ]);
    }

    /**
     * @Route("/category/{categoryId}", name="post_category", methods={"GET"})
     * @param PostRepository $postRepository
     * @param FavoriteRepository $favoriteRepository
     * @param CategoryRepository $categoryRepository
     * @param $categoryId
     * @return Response
     */
    public function categoryPosts(PostRepository $postRepository, FavoriteRepository $favoriteRepository,
                                  CategoryRepository $categoryRepository, $categoryId): Response
    {

        if(!$categoryRepository->find($categoryId)) {
            throw $this->createNotFoundException('This category does not exist');
        }

        $category = $categoryRepository->find($categoryId);

        $favorites = null;
        // admin is inMemory, so it does not have favorites
        if ($this->getUser() instanceof User) {
            $favorites = $favoriteRepository->findBy(['user' => $this->getUser()]);
        }

        return $this->render('post/index.html.twig', [
            'posts' => $postRepository->findBy(['category' => $category]),
            'favorites' => $favorites,
            'categories' => $categoryRepository->findAll(),
            'name' => $category->getName()
        ]);
    }

    /**
     * @Route("/subcategory/{subcategoryId}", name="post_subcategory", methods={"GET"})
     * @param PostRepository $postRepository
     * @param FavoriteRepository $favoriteRepository
     * @param SubcategoryRepository $subcategoryRepository
     * @param CategoryRepository $categoryRepository
     * @param $subcategoryId
     * @return Response
     */
    public function subcategoryPosts(PostRepository $postRepository, FavoriteRepository $favoriteRepository,
                                     SubcategoryRepository $subcategoryRepository, CategoryRepository $categoryRepository,
                                     $subcategoryId): Response
    {

        if(!$subcategoryRepository->find($subcategoryId)) {
            throw $this->createNotFoundException('This subcategory does not exist');
        }

        $subcategory = $subcategoryRepository->find($subcategoryId);

        $favorites = null;
        // admin is inMemory, so it does not have favorites
        if ($this->getUser() instanceof User) {
            $favorites = $favoriteRepository->findBy(['user' => $this->getUser()]);
        }

        return $this->render('post/index.html.twig', [
            'posts' => $postRepository->findBy(['subcategory' => $subcategory]),
            'favorites' => $favorites,
            'categories' => $categoryRepository->findAll(),
            'name' => $subcategory->getName()
        ]);
    }


    /**
     * @Route("/new", name="post_new", methods={"GET","POST"})
     * @param Request $request
     * @param SubcategoryRepository $subcategoryRepository
     * @param CategoryRepository $categoryRepository
     * @return Response
     */
    public function new(Request $request, SubcategoryRepository $subcategoryRepository, CategoryRepository $categoryRepository): Response
    {

        $post = new Post();
        $subcategories = $this->postManager->subcategoriesToArray();
        $categories = $this->postManager->categoriesToArray();

        // Create Form
        $form = $this->createForm(PostType::class, $post);


        $postImagesCollection = new ArrayCollection();
        foreach ($post->getPostImages() as $i) {
            $postImagesCollection->add($i);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $request->isXmlHttpRequest()) {


            $entityManager = $this->getDoctrine()->getManager();

            // get rid of the ones that the user got rid of in the interface (DOM)
            foreach ($postImagesCollection as $postImg) {
                if ($post->getPostImages()->contains($postImg) === false) {
                    $entityManager->remove($postImg);
                }
            }

            $post->setUser($this->getUser());
            $post->setCategory($categoryRepository->find($request->get('post_category')));
            $post->setSubcategory($subcategoryRepository->find($request->get('post_subcategory')));
            $post->setCreatedAt(time());

            $entityManager->persist($post);
            $entityManager->flush();

            return $this->redirectToRoute('post_index');

        }

        return $this->render('post/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
            'subcategoriesData' => $subcategories,
            'categoriesData' => $categories
        ]);
    }


    /**
     * @Route("/{id}", name="post_show", methods={"GET"})
     * @param PostRepository $postRepository
     * @param Post $post
     * @param ReviewRepository $reviewRepository
     * @param int $id
     * @return Response
     */
    public function show(PostRepository $postRepository,Post $post, ReviewRepository $reviewRepository, int $id): Response
    {

        if(!$postRepository->find($id)) {
            throw $this->createNotFoundException('This post does not exist');
        }

        return $this->render('post/show.html.twig', [
            'post' => $post,
            'reviews' => $reviewRepository->findBy(['post' => $post])
        ]);
    }

    /**
     * @Route("/{id}/edit", name="post_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Post $post
     * @param int $id
     * @param CategoryRepository $categoryRepository
     * @param SubcategoryRepository $subcategoryRepository
     * @param PostImageRepository $postImageRepository
     * @param PostRepository $postRepository
     * @return Response
     */
    public function edit(Request $request, Post $post, int $id,
                         CategoryRepository $categoryRepository, SubcategoryRepository $subcategoryRepository
                        , PostImageRepository $postImageRepository, PostRepository $postRepository): Response
    {

        if(!$postRepository->find($id)) {
            throw $this->createNotFoundException('This post does not exist');
        }

        $this->denyAccessUnlessGranted('edit', $post);

        $subcategories = $this->postManager->subcategoriesToArray();
        $categories = $this->postManager->categoriesToArray();
        $currentSubcategory = $post->getSubcategory()->getId();
        $currentCategory = $post->getCategory()->getId();

        $form = $this->createForm(PostType::class, $post);

        $postImagesCollection = new ArrayCollection();
        foreach ($post->getPostImages() as $i) {
            $postImagesCollection->add($i);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $request->isXmlHttpRequest()) {

            $entityManager = $this->getDoctrine()->getManager();

            if(!empty($request->get('deleteImages'))) {

                foreach($request->get('deleteImages') as $imgId) {
                    if ($imgId != 'undefined') {
                        $postImage = $postImageRepository->find($imgId);
                        $entityManager->remove($postImage);
                        $entityManager->flush();
                    }
                }
            }


            // get rid of the ones that the user got rid of in the interface (DOM)
            foreach ($postImagesCollection as $postImg) {
                if ($post->getPostImages()->contains($postImg) === false) {
                    $post->getPostImages()->removeElement($postImg);
                    $entityManager->remove($postImg);
                }
            }

            $post->setCategory($categoryRepository->find($request->get('post_category')));
            $post->setSubcategory($subcategoryRepository->find($request->get('post_subcategory')));

            $entityManager->persist($post);
            $entityManager->flush();

            return $this->redirectToRoute('post_index', [
                'id' => $post->getId(),
            ]);
        }

        return $this->render('post/edit.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
            'subcategoriesData' => $subcategories,
            'categoriesData' => $categories,
            'currentCategory' => $currentCategory,
            'currentSubcategory' => $currentSubcategory
        ]);
    }

    /**
     * @Route("/{id}/delete", name="post_delete", methods={"DELETE"})
     * @param Request $request
     * @param Post $post
     * @return Response
     */
    public function delete(Request $request, Post $post): Response
    {
        $this->denyAccessUnlessGranted('delete', $post);

        if ($this->isCsrfTokenValid('delete' . $post->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();

            foreach($post->getPostImages() as $img) {
                $entityManager->remove($img);
            }
            $entityManager->remove($post);
            $entityManager->flush();
        }

        return $this->redirectToRoute('post_index');
    }
}
