<?php

namespace App\Event;


use App\Entity\Message;
use Symfony\Component\EventDispatcher\Event;

class MessageSentEvent extends Event
{
    private $message;

    const NAME = 'message.sent';

    public function __construct(Message $message)
    {
        $this->message = $message;
    }


    public function getMessage()
    {
        return $this->message;
    }

    public function getSender()
    {
        return $this->message->getSender();
    }

    public function getRecipient()
    {
        return $this->message->getRecipient();
    }

}