/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.scss in this case)
require('../css/app.scss');



window.Popper = require('popper.js');
import 'bootstrap'

import 'popper.js';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'


// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');

// Breadcrumb

$(document).ready(function () {


    // Set Currency
    var $currency = $('#nav_currency');
    $currency.change(function () {
        var $form = $(this).closest('form').submit();
        var data = {};
        data[$currency.attr('name')] = $currency.val();
        $.ajax({
            url: $form.attr('action'),
            type: $form.attr('method'),
            data: data,
            success: function (html) {

            },
            failure: function(error)
            {
                console.log(error);
            }
        });
    });

    // sidebar menu show child elements on click

    $(".category-sidebar").on("click", function () {
        $(this).find("ul.sidebar-subcategories").toggleClass('sub');
    });

    var $this = $(this),
        $bc = $('<div class="item"></div>');

    $this.parents('li').each(function (n, li) {
        var $a = $(li).children('a').clone();
        $bc.prepend(' / ', $a);
    });


    // set breadcrumb by current path

    var pathArray = window.location.pathname.split('/');

    for (var i = 0; i < pathArray.length; i++) {

        if (pathArray[i] === 'post' && pathArray[i + 1] === 'all'
            || pathArray[i] === 'request' && pathArray[i + 1] === 'all') {
            $bc.append('<span class="bc breadcrumb-item">');
            $bc.append("<a href='/" + pathArray[i] + "/" + pathArray[i + 1] + "'>" + pathArray[i] + "</a>");
            $bc.append('</span>')
        }
        if (pathArray[i] === 'post' && pathArray[i + 1] === 'subcategory' || pathArray[i + 1] === 'category'
            || pathArray[i] === 'request' && pathArray[i + 1] === 'subcategory' || pathArray[i + 1] === 'category') {

            $bc.append('<span class="bc breadcrumb-item">');
            $bc.append("<a href='/" + pathArray[i] + '/all' + "'>" + pathArray[i] + "</a>");
            $bc.append('</span>');
            $bc.append('<span class="bc breadcrumb-item">');
            $bc.append("<a href='/" + pathArray[i] + "/" + pathArray[i + 1] + "/" + pathArray[i + 2] + "'>" + name + "</a>");
            $bc.append('</span>')
        }
        if (pathArray[i] === 'search') {
            $bc.append('<span class="bc breadcrumb-item">');
            $bc.append("<a href='#'>" + name + "</a>");
            $bc.append('</span>')
        }


    }

    $('.breadcrumb').html($bc.prepend('<a href="/">Home</a>'));


// messenger.twig.html
// send message by email
    $('#msgAdd').click(function () {
        $(this).closest('form').submit();
    });


    $(window).scroll(function (event) {
        var height = $(window).scrollTop();

        if(height  > 600) {
            $('nav').addClass("navHomeAnimation");
        }
        if(height < 600) {
            $('nav').removeClass("navHomeAnimation");
        }

    });

    $('.carousel').carousel();

    $('.carousel').carousel({
        interval: 2000
    });





    if (window.location.href.indexOf("message") > -1) {



        function autocompleter(inp, arr) {
            /*the autocomplete function takes two arguments,
            the text field element and an array of possible autocompleted values:*/
            var currentFocus;
            /*execute a function when someone writes in the text field:*/
            inp.addEventListener("input", function (e) {
                var a, b, i, val = this.value;
                /*close any already open lists of autocompleted values*/
                closeAllLists();
                if (!val) {
                    return false;
                }
                currentFocus = -1;
                /*create a DIV element that will contain the items (values):*/
                a = document.createElement("DIV");
                a.setAttribute("id", this.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");
                /*append the DIV element as a child of the autocomplete container:*/
                this.parentNode.appendChild(a);
                /*for each item in the array...*/
                for (i = 0; i < arr.length; i++) {
                    /*check if the item starts with the same letters as the text field value:*/
                    if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                        /*create a DIV element for each matching element:*/
                        b = document.createElement("DIV");
                        /*make the matching letters bold:*/
                        b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                        b.innerHTML += arr[i].substr(val.length);
                        /*insert a input field that will hold the current array item's value:*/
                        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                        /*execute a function when someone clicks on the item value (DIV element):*/
                        b.addEventListener("click", function (e) {
                            /*insert the value for the autocomplete text field:*/
                            inp.value = this.getElementsByTagName("input")[0].value;
                            /*close the list of autocompleted values,
                            (or any other open lists of autocompleted values:*/
                            closeAllLists();
                        });
                        a.appendChild(b);
                    }
                }
            });
            /*execute a function presses a key on the keyboard:*/
            inp.addEventListener("keydown", function (e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                    /*If the arrow DOWN key is pressed,
                    increase the currentFocus variable:*/
                    currentFocus++;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 38) { //up
                    /*If the arrow UP key is pressed,
                    decrease the currentFocus variable:*/
                    currentFocus--;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 13) {
                    /*If the ENTER key is pressed, prevent the form from being submitted,*/
                    e.preventDefault();
                    if (currentFocus > -1) {
                        /*and simulate a click on the "active" item:*/
                        if (x) x[currentFocus].click();
                    }
                }
            });

            function addActive(x) {
                /*a function to classify an item as "active":*/
                if (!x) return false;
                /*start by removing the "active" class on all items:*/
                removeActive(x);
                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
                /*add class "autocomplete-active":*/
                x[currentFocus].classList.add("autocomplete-active");
            }

            function removeActive(x) {
                /*a function to remove the "active" class from all autocomplete items:*/
                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
            }

            function closeAllLists(elmnt) {
                /*close all autocomplete lists in the document,
                except the one passed as an argument:*/
                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != inp) {
                        x[i].parentNode.removeChild(x[i]);
                    }
                }
            }
            
            

            /*execute a function when someone clicks in the document:*/
            document.addEventListener("click", function (e) {
                closeAllLists(e.target);
            });
        }

        autocompleter(document.getElementById("msgAddInput"), usersEmails);


    }

}); // jquery end


