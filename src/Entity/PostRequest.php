<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRequestRepository")
 */
class PostRequest
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="postRequests")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2, nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $deliveryTimeFrom;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $deliveryTimeTo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $deliveryTimeFormat;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="postRequests")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subcategory", inversedBy="postRequests")
     * @ORM\JoinColumn(nullable=false)
     */
    private $subcategory;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PostImage", mappedBy="postRequest", cascade={"persist"})
     */
    private $postImages;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     */
    private $currency;

    public function __construct()
    {
        $this->postImages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }


    public function getDeliveryTimeFrom(): ?int
    {
        return $this->deliveryTimeFrom;
    }

    public function setDeliveryTimeFrom(?int $deliveryTimeFrom): self
    {
        $this->deliveryTimeFrom = $deliveryTimeFrom;

        return $this;
    }

    public function getDeliveryTimeTo(): ?int
    {
        return $this->deliveryTimeTo;
    }

    public function setDeliveryTimeTo(?int $deliveryTimeTo): self
    {
        $this->deliveryTimeTo = $deliveryTimeTo;

        return $this;
    }

    public function getDeliveryTimeFormat(): ?string
    {
        return $this->deliveryTimeFormat;
    }

    public function setDeliveryTimeFormat(?string $deliveryTimeFormat): self
    {
        $this->deliveryTimeFormat = $deliveryTimeFormat;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getSubcategory(): ?Subcategory
    {
        return $this->subcategory;
    }

    public function setSubcategory(?Subcategory $subcategory): self
    {
        $this->subcategory = $subcategory;

        return $this;
    }


    /**
     * @return Collection|PostImage[]
     */
    public function getPostImages(): Collection
    {
        return $this->postImages;
    }

    public function addPostImage(PostImage $postImage): self
    {
        if (!$this->postImages->contains($postImage)) {
            $this->postImages[] = $postImage;
            $postImage->setPostRequest($this);
        }

        return $this;
    }

    public function removePostImage(PostImage $postImage): self
    {
        if ($this->postImages->contains($postImage)) {
            $this->postImages->removeElement($postImage);
            // set the owning side to null (unless already changed)
            if ($postImage->getPostRequest() === $this) {
                $postImage->setPostRequest(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?int
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?int $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?int
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?int $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }
}
