<?php


namespace App\Service;


use App\Entity\Post;
use App\Entity\PostImage;
use App\Entity\PostRequest;
use App\Repository\PostImageRepository;

class PostImageManager
{

    private $postImageRepository;


    public function __construct(PostImageRepository $postImageRepository)
    {
        $this->postImageRepository = $postImageRepository;
    }

    public function postMainImage(Post $post = null) : ?PostImage
    {
        if($post == null) {
            return null;
        }
        return $this->postImageRepository->findOneBy(['post' => $post]);
    }

    public function postRequestMainImage(PostRequest $postRequest = null) : ?PostImage
    {
        if($postRequest == null) {
            return null;
        }
        return $this->postImageRepository->findOneBy(['postRequest' => $postRequest]);
    }

    public function getPostImages(Post $post = null) : ?array
    {
        if($post == null) {
            return null;
        }

        return $this->postImageRepository->findBy(['post' => $post]);
    }

    public function getPostRequestImages(PostRequest $postRequest = null) : ?array
    {
        if($postRequest == null) {
            return null;
        }

        return $this->postImageRepository->findBy(['postRequest' => $postRequest]);
    }
}