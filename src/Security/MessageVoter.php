<?php

namespace App\Security;

use App\Entity\Message;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class MessageVoter extends Voter
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';
    const GET_RECIPIENT = 'getRecipient';

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, [self::VIEW, self::EDIT, self::DELETE, self::GET_RECIPIENT])) {
            return false;
        }

        // only vote on Message objects inside this voter
        if (!$subject instanceof Message) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        $user->getId();

        if(!$this->userRepository->find($attribute))
        {
            return false;
        }

        return true;


    }
}