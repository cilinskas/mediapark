<?php


namespace App\Security;


use App\Entity\Post;
use App\Repository\PostRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class PostVoter extends Voter
{

    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    private $security;
    private $postRepository;

    public function __construct(Security $security, PostRepository $postRepository)
    {
        $this->security = $security;
        $this->postRepository = $postRepository;
    }

    protected function supports($attribute, $subject)
    {

        if (!in_array($attribute, [self::VIEW, self::EDIT, self::DELETE])) {
            return false;
        }

        if (!$subject instanceof Post) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {

        $currentUser = $token->getUser();
        $user = $subject->getUser();

        switch ($attribute) {
            case 'edit':
                if ($user == $currentUser) {
                    return true;
                }
                if ($this->security->isGranted('ROLE_SUPER_ADMIN')) {
                    return true;
                }
                return false;
            case 'delete':
                if ($user == $currentUser) {
                    return true;
                }
                if ($this->security->isGranted('ROLE_SUPER_ADMIN')) {
                    return true;
                }
                return false;
        }

        return false;
    }

}