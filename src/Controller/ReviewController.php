<?php

namespace App\Controller;

use App\Entity\Review;
use App\Repository\PostRepository;
use App\Service\ReviewManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/review")
 */
class ReviewController extends AbstractController
{

    private $reviewManager;

    public function __construct(ReviewManager $reviewManager)
    {
        $this->reviewManager = $reviewManager;
    }

    /**
     * @Route("/new/{id}", name="review_new", methods={"GET","POST"})
     * @param Request $request
     * @param PostRepository $postRepository
     * @param $id
     * @return RedirectResponse
     */
    public function new(Request $request, PostRepository $postRepository, $id): RedirectResponse
    {
        $referer = $request->headers->get('referer');

        // allow only one review for one user
        if(!$this->reviewManager->allowOne($id,$this->getUser())) {
            $this->addFlash('error',"You've already written a review for this post");
            return new RedirectResponse($referer);
        }

        $rating = $request->get('rating');
        $description = $request->get('description');

        if($rating && $description) {
            $review = new Review();
            $review->setRating($rating);
            $review->setDescription($description);
            $review->setUser($this->getUser());
            $review->setPost($postRepository->find($id));
            $review->setCreatedAt(time());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($review);
            $entityManager->flush();
            return new RedirectResponse($referer);
        } else {
            $this->addFlash('error','Some fields are empty');
            return new RedirectResponse($referer);
        }

    }



}