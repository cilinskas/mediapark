<?php

namespace App\Service;


use App\Repository\FavoriteRepository;
use App\Repository\PostRepository;
use Symfony\Component\Security\Core\Security;

class FavoriteManager
{

    private $security;
    private $favoriteRepository;
    private $postRepository;

    public function __construct(Security $security, FavoriteRepository $favoriteRepository, PostRepository $postRepository)
    {
        $this->security = $security;
        $this->favoriteRepository = $favoriteRepository;
        $this->postRepository = $postRepository;
    }


    public function getFavoritePosts() : ?array
    {
        $favorites = $this->getFavorites();
        $posts = [];

        foreach ($favorites as $favorite) {
            $posts[] = $favorite->getPost();
        }

        return $posts;
    }

    public function getFavorites() : ?array
    {
        return $this->favoriteRepository->findBy(['user' => $this->security->getUser()]);
    }




}