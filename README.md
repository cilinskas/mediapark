## Installation

Install docker and docker-compose


Just copy files from this repository to your project main directory

## Launch Project

 ```bash
 $ scripts/start-dev.sh
 ```
 
 ```bash
 $ scripts/backend.sh
 
 $ composer install
 
 $ yarn add @symfony/webpack-encore --dev
  ```
  
  ```bash
  $ php bin/console doctrine:migrations:migrate
  
  $ php bin/console doctrine:fixtures:load --group=AppFixtures
  ```

## Development
Frontend
  ```bash
  $ yarn encore dev --watch
  ```
  

## Database Structure
 
UML diagram located in docs folder

## Super Admin Login
 
email: admin@admin.lt

password: kitten