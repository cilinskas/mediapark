


$(document).ready(function () {

    var $currency = $('#nav_currency');
// When currency gets selected ...
    $currency.change(function () {
        // ... retrieve the corresponding form.
        var $form = $(this).closest('form').submit();
        // Simulate form data, but only include the selected currency value.
        var data = {};
        data[$currency.attr('name')] = $currency.val();
        // Submit data via AJAX to the form's action path.
        $.ajax({
            url: $form.attr('action'),
            type: $form.attr('method'),
            data: data,
            success: function (html) {

            }
        });
    });

});