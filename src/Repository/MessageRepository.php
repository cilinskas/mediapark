<?php

namespace App\Repository;

use App\Entity\Message;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Message::class);
    }

    /**
     * Returns the messages received by a given user.
     *
     * @param $recipient int
     * @return array
     */
    public function findMessagesByRecipient($recipient)
    {
        $query = $this->createQueryBuilder('m')
            ->join('m.recipient', 'u')
            ->where('u.id = :recipient')
            ->setParameter('recipient', $recipient)
            ->orderBy('m.sentAt', 'ASC')
            ->select('m', 'u')
            ->getQuery();
        return $query->getResult();
    }


    /**
     * Returns the messages received by a given user.
     *
     * @param $sender int
     * @return array
     */
    public function findMessagesBySender($sender)
    {
        $query = $this->createQueryBuilder('m')
            ->join('m.sender', 'u')
            ->where('u.id = :sender')
            ->setParameter('sender', $sender)
            ->orderBy('m.sentAt', 'ASC')
            ->select('m', 'u')
            ->getQuery();
        return $query->getResult();
    }

    public function findMessagesBySenderAndRecipient($sender, $recipient)
    {
        $query = $this->createQueryBuilder('m')
            ->join('m.sender', 's')
            ->join('m.recipient', 'r')
            ->where('m.sender = (:sender)')
            ->andWhere('m.recipient = (:recipient)')
            ->setParameter('sender', $sender)
            ->setParameter('recipient', $recipient)
            ->orderBy("m.sentAt", "asc")
            ->getQuery();
        return $query->getResult();
    }

    public function findMessagesByRecipientAndSender($sender, $recipient)
    {
        $query = $this->createQueryBuilder('m')
            ->join('m.sender', 's')
            ->join('m.recipient', 'r')
            ->where('m.sender = (:sender)')
            ->andWhere('m.recipient = (:recipient)')
            ->setParameter('sender', $recipient)
            ->setParameter('recipient', $sender)
            ->orderBy("m.sentAt", "asc")
            ->getQuery();
        return $query->getResult();
    }



}
