<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostImageRepository")
 * @Vich\Uploadable
 */
class PostImage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Post", inversedBy="postImages", cascade={"persist"})
     */
    private $post;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="postImages", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var string
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PostRequest", inversedBy="postImages", cascade={"persist"})
     */
    private $postRequest;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPost(): ?Post
    {
        return $this->post;
    }

    public function setPost(?Post $post): self
    {
        $this->post = $post;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            if(!$this->getUpdatedAt()) {
                $this->createdAt = time();
                $this->updatedAt = new \DateTime('now');
            } else {
                $this->updatedAt = new \DateTime('now');
            }
        }
    }

    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime('now');
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function getPostRequest(): ?PostRequest
    {
        return $this->postRequest;
    }

    public function setPostRequest(?PostRequest $postRequest): self
    {
        $this->postRequest = $postRequest;

        return $this;
    }

    public function getCreatedAt(): ?int
    {
        return $this->createdAt;
    }

    public function setCreatedAt(int $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
