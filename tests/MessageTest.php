<?php

namespace App\Tests;


use App\Entity\Message;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MessageTest extends WebTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    private $user;
    private $currency;
    private $post;
    private $category;
    private $subcategory;
    private $sender;
    private $recipient;

    public function setUp()
    {
        self::bootKernel();
        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $schemaTool = new SchemaTool($this->em);
        $metadata = $this->em->getMetadataFactory()->getAllMetadata();

        $schemaTool->dropSchema($metadata);
        $schemaTool->createSchema($metadata);

        $loader = new Loader();
        $loader->loadFromDirectory('src/DataFixtures/Tests');

        $purger = new ORMPurger();
        $executor = new ORMExecutor($this->em, $purger);
        $executor->execute($loader->getFixtures(), true);

        $this->user = $this->em->getRepository('App\Entity\User')->findOneBy(['id' => 1]);
        $this->sender = $this->em->getRepository('App\Entity\User')->findOneBy(['id' => 10]);
        $this->recipient = $this->em->getRepository('App\Entity\User')->findOneBy(['id' => 2]);
        $this->currency = $this->em->getRepository('App\Entity\Currency')->findOneBy(['id' => 1]);
        $this->post = $this->em->getRepository('App\Entity\Post')->findOneBy(['id' => 1]);
        $this->category = $this->em->getRepository('App\Entity\Category')->findOneBy(['id' => 1]);
        $this->subcategory = $this->em->getRepository('App\Entity\Subcategory')->findOneBy(['id' => 1]);

    }

    public function testSendMessageAndCheckIfCanBeFoundAsInquiry()
    {

        $message = new Message();
        $message->setMessage('message1x');
        $message->setSender($this->sender);
        $message->setRecipient($this->recipient);
        $message->setSentAt(time());

        $this->em->persist($message);
        $this->em->flush();

        $message = $this->em->getRepository('App\Entity\Message')->findOneBy(['message' => 'message1x']);

        $this->assertEquals('message1x', $message->getMessage());

        $messagesBySender = $this->em->getRepository('App\Entity\Message')->findMessagesBySenderAndRecipient($this->sender,$this->recipient);


        $inquiries = [];


        foreach ($messagesBySender as $m) {
            if (!in_array($m->getRecipient(), $inquiries)) {
                array_push($inquiries, $m->getRecipient());
            }
        }

        $this->assertContains($message->getRecipient(),$inquiries);

        $this->em->remove($message);
        $this->em->flush();


    }



    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();
        $this->em->close();
    }





}