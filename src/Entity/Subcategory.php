<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubcategoryRepository")
 */
class Subcategory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="subcategories")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="subcategory")
     */
    private $posts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PostRequest", mappedBy="subcategory")
     */
    private $postRequests;

    /**
     * @ORM\Column(type="integer")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $updatedAt;


    public function __construct()
    {
        $this->posts = new ArrayCollection();
        $this->postRequests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCategory(): ?category
    {
        return $this->category;
    }

    public function setCategory(?category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setSubcategory($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);

            // set the owning side to null (unless already changed)

            if ($post->getSubcategory() === $this) {
                $post->setSubcategory(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return Collection|PostRequest[]
     */
    public function getPostRequests(): Collection
    {
        return $this->postRequests;
    }

    public function addPostRequest(PostRequest $postRequest): self
    {
        if (!$this->postRequests->contains($postRequest)) {
            $this->postRequests[] = $postRequest;
            $postRequest->setSubcategory($this);
        }

        return $this;
    }

    public function removePostRequest(PostRequest $postRequest): self
    {
        if ($this->postRequests->contains($postRequest)) {
            $this->postRequests->removeElement($postRequest);
            // set the owning side to null (unless already changed)
            if ($postRequest->getSubcategory() === $this) {
                $postRequest->setSubcategory(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?int
    {
        return $this->createdAt;
    }

    public function setCreatedAt(int $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?int
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?int $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

}
