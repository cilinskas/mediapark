<?php

namespace App\Event;

use Swift_Mailer;
use Swift_Message;
use Symfony\Component\EventDispatcher\Tests\Debug\EventSubscriber;

class MessageSentSubscriber extends EventSubscriber
{

    public static function getSubscribedEvents()
    {
        return [
          MessageSentEvent::NAME => 'onMessageSent'
        ];
    }

    public function onMessageSent(MessageSentEvent $event, Swift_Mailer $mailer)
    {

        $message = (new Swift_Message('Hello Email'))
            ->setFrom($event->getSender()->getEmail())
            ->setTo($event->getRecipient()->getEmail())
            ->setBody(
                $this->renderView(
                    'emails/message.html.twig',
                    ['name' => $event->getRecipient()->getEmail()]
                ),
                'text/html'
            )
        ;

        $mailer->send($message);

    }



}