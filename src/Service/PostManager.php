<?php


namespace App\Service;


use App\Repository\CategoryRepository;
use App\Repository\SubcategoryRepository;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

class PostManager
{

    private $subcategoryRepository;
    private $categoryRepository;

    public function __construct(SubcategoryRepository $subcategoryRepository, CategoryRepository $categoryRepository)
    {
        $this->subcategoryRepository = $subcategoryRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function subcategoriesToArray() : array
    {
        $subcategories = [];
        foreach($this->subcategoryRepository->findAll() as $subcategory)
        {
            $item = array(
                'name' => $subcategory->getName(),
                'category' => $subcategory->getCategory()->getId(),
                'id' => $subcategory->getId()
            );
            $subcategories[] = $item;
        }

        return $subcategories;
    }

    public function categoriesToArray() : array
    {
        $categories = [];
        foreach($this->categoryRepository->findAll() as $category)
        {
            $item = array(
                'name' => $category->getName(),
                'id' => $category->getId()
            );
            $categories[] = $item;
        }

        return $categories;
    }

    public function mountDefaultImage() : void
    {
        $filesystem = new Filesystem();

        try {
            $filesystem->copy(__DIR__ . '/../../public/images/starsCopy.png', __DIR__ . '/../../public/images/stars.png', true);
        } catch (IOExceptionInterface $exception) {
            echo "An error occurred while creating your directory at " . $exception->getPath();
        }
    }

}