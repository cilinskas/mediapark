$(document).ready(function () {

// New Post Dynamic Data

    var form = document.getElementById('newPostRequest');
    var subcategoryInput, categoryInput;

    categoryInput = document.createElement("select");
    categoryInput.id = "post_request_category";
    categoryInput.setAttribute('class', 'form-control');
    categoryInput.setAttribute('name', 'post_request_category');
    categoryInput.required = true;
    form.insertBefore(categoryInput, form.children[7]);

    $('#post_request_category').append('<option selected="selected" disabled>Select Category</option>');
    categoriesData.forEach(function (category) {
        $('#post_request_category').append($('<option>', {value: category['id'], text: category['name']}));
    });

    if (window.location.href.indexOf("edit") > -1) {
        $("#post_request_category option[value=" + currentCategory + "]").attr("selected", true);
    }


//Create and append select list
    subcategoryInput = document.createElement("select");
    subcategoryInput.id = "post_request_subcategory";
    subcategoryInput.setAttribute('class', 'form-control');
    subcategoryInput.setAttribute('name', 'post_request_subcategory');
    if (window.location.href.indexOf("new") > -1) {
        subcategoryInput.setAttribute('style', 'opacity:0');
    }
    subcategoryInput.required = true;
    form.insertBefore(subcategoryInput, form.children[8]);

    var $category = $('#post_request_category');

    if (window.location.href.indexOf("edit") > -1) {
        setSubcategory();
        $("#post_request_subcategory option[value=" + currentSubcategory + "]").attr("selected", true);
    }


    $category.change(function () {
        $("#post_request_subcategory").animate({opacity: 1}, 300);
        setSubcategory();
    });

    function setSubcategory() {
        //get current category id
        var categoryInput = document.getElementById('post_request_category');
        var categoryId = parseInt(categoryInput.options[categoryInput.selectedIndex].value);

        // set initial subcategories
        var subcategories = [];
        for (var i = 0; i < 16; i++) {
            if (subcategoriesData[i]['category'] === categoryId) {
                var obj = {
                    name: subcategoriesData[i]['name'],
                    category: subcategoriesData[i]['category'],
                    id: subcategoriesData[i]['id']
                };
                subcategories.push(obj);
            }
        }

        $('#post_request_subcategory').find('option').remove();

        subcategories.forEach(function (subcategory) {
            $('#post_request_subcategory').append($('<option>', {value: subcategory['id'], text: subcategory['name']}));
        });
    }

    $('#post_request_postImages').parent().hide();


    // this variable is the list in the dom, it's initiliazed when the document is ready
    var $collectionHolder;
// the link which we click on to add new items
    var $addNewItem = $('<a href="#" class="btn btn-info">Add new item</a>');
// when the page is loaded and ready
    $(document).ready(function () {
        // get the collectionHolder, initilize the var by getting the list;
        $collectionHolder = $('#imagesList');
        // append the add new item link to the collectionHolder
        $collectionHolder.append($addNewItem);
        // add an index property to the collectionHolder which helps track the count of forms we have in the list
        $collectionHolder.data('index', $collectionHolder.find('.panel').length)
        // finds all the panels in the list and foreach one of them we add a remove button to it
        // add remove button to existing items
        $collectionHolder.find('.panel').each(function () {
            // $(this) means the current panel that we are at
            // which means we pass the panel to the addRemoveButton function
            // inside the function we create a footer and remove link and append them to the panel
            // more informations in the function inside
            addRemoveButton($(this));
        });
        // handle the click event for addNewItem
        $addNewItem.click(function (e) {
            // preventDefault() is your  homework if you don't know what it is
            // also look up preventPropagation both are usefull
            e.preventDefault();
            // create a new form and append it to the collectionHolder
            // and by form we mean a new panel which contains the form
            addNewForm();
        })
    });

    /*
     * creates a new form and appends it to the collectionHolder
     */
    function addNewForm() {
        // getting the prototype
        // the prototype is the form itself, plain html
        var prototype = $collectionHolder.data('prototype');
        // get the index
        // this is the index we set when the document was ready, look above for more info
        var index = $collectionHolder.data('index');
        // create the form
        var newForm = prototype;
        // replace the __name__ string in the html using a regular expression with the index value
        newForm = newForm.replace(/__name__/g, index);
        // incrementing the index data and setting it again to the collectionHolder
        $collectionHolder.data('index', index + 1);
        // create the panel
        // this is the panel that will be appending to the collectionHolder
        var $panel = $('<div class="panel panel-warning"><div class="panel-heading"></div></div>');
        // create the panel-body and append the form to it
        var $panelBody = $('<div class="panel-body"></div>').append(newForm);
        // append the body to the panel
        $panel.append($panelBody);
        // append the removebutton to the new panel
        addRemoveButton($panel);
        // append the panel to the addNewItem
        // we are doing it this way to that the link is always at the bottom of the collectionHolder
        $addNewItem.before($panel);

        let formHeight = $( '#newPostRequest' ).height();
        $('.postPageFormBg').css({"height": formHeight+200});
        $('.postNewPage').css({"height": formHeight+300});
    }

    /**
     * adds a remove button to the panel that is passed in the parameter
     * @param $panel
     */
    function addRemoveButton($panel) {
        // create remove button
        var $removeButton = $('<a href="#" class="btn btn-danger">Remove</a>');
        // appending the removebutton to the panel footer
        var $panelFooter = $('<div class="panel-footer"></div>').append($removeButton);
        // handle the click event of the remove button
        $removeButton.click(function (e) {
            e.preventDefault();
            // gets the parent of the button that we clicked on "the panel" and animates it
            // after the animation is done the element (the panel) is removed from the html
            // idToDelete = [];
            id = $(this).parents('.panel').find('.oldImages').val();
            if($(id)){
                $('#newPostRequest').append('<input name="deleteImages[]" type="hidden" value="'+id+'">');
            }

            $(e.target).parents('.panel').slideUp(1000, function () {
                $(this).remove();
            });

            let formHeight = $( '#newPostRequest' ).height();
            $('.postPageFormBg').css({"height": formHeight+200});
            $('.postNewPage').css({"height": formHeight+300});
        });
        // append the footer to the panel
        $panel.append($panelFooter);
    }

    $(".custom-file-input").change(function () {

        if (this.files[0].size > 2000000) {
            alert("Please upload file less than 2MB.");
            $(this).val('');
        }

    });

    let formHeight = $( '#newPostRequest' ).height();
    $('.postPageFormBg').css({"height": formHeight+200});
    $('.postNewPage').css({"height": formHeight+300});



    $("form").submit(function (e) {

        e.preventDefault();

        var $form = $(this).closest('form');
        var url = $form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: new FormData($('#newPostRequest')[0]),
            processData: false,
            contentType: false
        }).done(function (data) {
            $('body').html(data);
            top.location.href = '/';
        }).fail(function (xhr, status, error) {
            $('body').html(xhr.responseText);
        });


    });


});