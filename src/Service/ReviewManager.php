<?php


namespace App\Service;

use App\Entity\Post;
use App\Entity\User;

class ReviewManager
{

    public function reviewAvg(Post $post): float
    {

        $reviews = $post->getReviews();

        if (count($reviews) > 0) {
            $reviewsAmount = count($reviews);
            $reviewAvg = 0;

            foreach ($reviews as $review) {
                $reviewAvg += $review->getRating();
            }

            $reviewAvg = $reviewAvg / $reviewsAmount;

            return $reviewAvg;

        } else {
            return 0;
        }

    }


    public function reviewCount(Post $post): int
    {
        return count($post->getReviews());
    }

    public function allowOne(int $id, User $user) : bool
    {
        $userReviews = $user->getReviews();

        foreach ($userReviews as $userReview) {
            if ($userReview->getPost()->getId() == $id) {
                return false;
            }
        }

        return true;
    }

}