<?php

namespace App\Twig;

use App\Entity\Post;
use App\Entity\PostImage;
use App\Entity\PostRequest;
use App\Repository\PostImageRepository;
use App\Service\CurrencyManager;
use App\Service\PostImageManager;
use App\Service\ReviewManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{

    private $currencyManager;
    private $reviewManager;
    private $postImageManager;


    public function __construct(CurrencyManager $currencyManager, ReviewManager $reviewManager, PostImageManager $postImageManager)
    {
        $this->currencyManager = $currencyManager;
        $this->reviewManager = $reviewManager;
        $this->postImageManager = $postImageManager;
    }


    public function getFunctions()
    {
        return [
            new TwigFunction('priceRate', [$this, 'priceRate']),
            new TwigFunction('reviewAvg', [$this, 'reviewAvg']),
            new TwigFunction('reviewCount', [$this, 'reviewCount']),
            new TwigFunction('postMainImage', [$this, 'postMainImage']),
            new TwigFunction('postRequestMainImage', [$this, 'postRequestMainImage']),
            new TwigFunction('getPostImages', [$this, 'getPostImages']),
            new TwigFunction('getPostRequestImages', [$this, 'getPostRequestImages']),
            new TwigFunction('instanceof', [$this, 'instanceof']),
        ];
    }


    /**
     * @param float $amount
     * @param string $from
     * @param string $to
     * @return float
     */
    public function priceRate(float $amount, string $from, string $to) : float
    {
       return $this->currencyManager->priceRate($amount,$from,$to);
    }


    /**
     * @param Post $post
     * @return float
     */
    public function reviewAvg(Post $post) : float
    {
        return $this->reviewManager->reviewAvg($post);
    }

    /**
     * @param Post $post
     * @return int
     */
    public function reviewCount(Post $post) : int
    {
        return $this->reviewManager->reviewCount($post);
    }

    public function postMainImage(Post $post = null) : ?PostImage
    {
        return $this->postImageManager->postMainImage($post);
    }

    public function postRequestMainImage(PostRequest $postRequest = null) : ?PostImage
    {
        return $this->postImageManager->postRequestMainImage($postRequest);
    }

    public function getPostImages(Post $post = null) : ?array
    {
        return $this->postImageManager->getPostImages($post);
    }

    public function getPostRequestImages(PostRequest $postRequest = null) : ?array
    {
        return $this->postImageManager->getPostRequestImages($postRequest);
    }

    public function instanceof($var, $instance) : bool {
        return  $var instanceof $instance;
    }




}