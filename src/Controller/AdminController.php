<?php

namespace App\Controller;


use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 *
 */
class AdminController extends AbstractController
{

    /**
     * @Route("/", name="admin_index")
     */
    public function index()
    {
        return $this->render('admin/home/index.html.twig');
    }


    /**
     * @Route("/user", name="admin_user")
     * @param UserRepository $userRepository
     * @return Response
     */
    public function manageUser(UserRepository $userRepository)
    {
        return $this->render('admin/user/manage.html.twig',[
            'users' => $userRepository->findAll()
        ]);
    }


}