<?php


namespace App\Service;


use App\Repository\UserRepository;

class UserManager
{

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getUsersEmails()
    {
        $emails = [];
        foreach($this->userRepository->findAll() as $user)
        {
            $emails[] = $user->getEmail();
        }

        return $emails;
    }

}