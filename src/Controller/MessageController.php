<?php

namespace App\Controller;

use App\Entity\Message;
use App\Event\MessageSentEvent;
use App\Repository\UserRepository;
use App\Service\MessageManager;
use App\Service\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/message")
 */
class MessageController extends AbstractController
{

    private $messageManager;
    private $userManager;


    public function __construct(MessageManager $messageManager, UserManager $userManager)
    {
        $this->messageManager = $messageManager;
        $this->userManager = $userManager;
    }

    /**
     * @Route("/dashboard", name="message_dashboard", methods={"GET"})
     * @return Response
     */
    public function messengerDashboard(): Response
    {
       $groupedMessages = $this->messageManager->getGroupedMessages();
       $usersEmails = $this->userManager->getUsersEmails();

        return $this->render('/message/messenger.html.twig', [
            'groupedMessages' => $groupedMessages,
            'usersEmails' => $usersEmails
        ]);

    }

    /**
     * @Route("/{senderId}", name="message_chat", methods={"GET"})
     * @param UserRepository $userRepository
     * @param int $senderId
     * @return Response
     */
    public function openMessengerChat(UserRepository $userRepository, int $senderId) : Response
    {
        if(!$userRepository->find($senderId)) {
            throw $this->createNotFoundException('This user does not exist');
        }

        $usersEmails = $this->userManager->getUsersEmails();
        $messages = $this->messageManager->getChatMessages($senderId);
        $groupedMessages = $this->messageManager->getGroupedMessages();

        return $this->render('/message/messenger.html.twig', [
            'sender' => $userRepository->find($senderId),
            'messages' => $messages,
            'receiver' => $this->getUser(),
            'groupedMessages' => $groupedMessages,
            'usersEmails' => $usersEmails
        ]);

    }


    /**
     * @Route("/sendMessage", name="message_sendMessage")
     * @param Request $request
     * @param UserRepository $userRepository
     * @param EventDispatcherInterface $eventDispatcher
     * @return RedirectResponse
     */
    public function sendMessage(Request $request, UserRepository $userRepository, EventDispatcherInterface $eventDispatcher) : RedirectResponse
    {

        $messageContent = $request->get('message');
        $recipientId = $request->get('recipientId');

        $recipient = $userRepository->find($recipientId);
        if (!$recipient) {
            throw $this->createNotFoundException('This recipient does not exist');
        }

            if (!empty($messageContent)) {

                $message = new Message();
                $message->setMessage($messageContent);
                $message->setSender($this->getUser());
                $message->setRecipient($userRepository->find($recipientId));
                $message->setSentAt(time());
                $message->setCreatedAt(time());

                $em = $this->getDoctrine()->getManager();
                $em->persist($message);
                $em->flush();

                $event = new MessageSentEvent($message);
                $eventDispatcher->dispatch(MessageSentEvent::NAME, $event);

                return $this->redirectToRoute('message_chat', ['senderId' => $recipientId]);

            } else {
                $this->addFlash('error', 'message content is empty');
                return $this->redirectToRoute('message_dashboard');
            }

    }

    /**
     * @Route("/checkUser", name="message_checkUser")
     * @param Request $request
     * @param UserRepository $userRepository
     * @return Response
     */
    public function checkUser(Request $request, UserRepository $userRepository) : Response
    {
        $email = $request->get('user_email');

        $user = $userRepository->findOneBy(['email'=>$email]);

        if(!$user) {
            $this->addFlash('error', 'No such user with email "'.$email. '" found');
            return $this->redirectToRoute('message_dashboard');
        } else {
            return $this->redirectToRoute('message_chat',['senderId' => $user->getId()]);
        }


    }



}