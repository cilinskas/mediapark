<?php

namespace App\Service;

class CurrencyManager{

    /**
     * @param float|int $amount
     * @param string $from
     * @param string $to
     * @return float
     */
    public function priceRate(float $amount, string $from, string $to) : float
    {

        if ($amount && $from && $to) {

            $apikey = getenv('CURRENCY_API_KEY');
            $api = urlencode($apikey);
            $from_Currency = urlencode($from);
            $to_Currency = urlencode($to);
            $query = "$from_Currency" . "_" . "$to_Currency";

            $json = file_get_contents("https://free.currencyconverterapi.com/api/v6/convert?q=$query&compact=ultra&apiKey=$api");
            $obj = json_decode($json, true);

            $val = $obj[$query];

            $total = $val * $amount;

            return number_format($total, 2, '.', '');

        } else {
            return null;
        }

    }

}