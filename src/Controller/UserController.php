<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserEditType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{

    /**
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('user/index.html.twig');
    }


    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     * @param Request $request
     * @param User $user
     * @param $id
     * @return Response
     */
    public function edit(Request $request, User $user, int $id): Response
    {

        $this->denyAccessUnlessGranted('edit',$user);

        $form = $this->createForm(UserEditType::class, $user);

        if ($this->isGranted('ROLE_SUPER_ADMIN', $user)) {

            $form->add('roles', ChoiceType::class, [
                'multiple' => true,
                'expanded' => true,
                'required' => false,
                'label' => 'Make This User Admin',
                'choices' => [
                    'Admin' => 'ROLE_USER_ADMIN'
                ]
            ]);

        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getManager()->flush();

            if ($this->isGranted('ROLE_SUPER_ADMIN', $user)) {
                return $this->redirectToRoute('admin_user', [
                    'id' => $user->getId(),
                ]);
            } else {
                return $this->redirectToRoute('user_index', [
                    'id' => $user->getId(),
                ]);
            }


        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home');
    }
}
