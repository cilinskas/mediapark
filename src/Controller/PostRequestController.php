<?php

namespace App\Controller;

use App\Entity\PostRequest;
use App\Form\PostRequestType;
use App\Repository\CategoryRepository;
use App\Repository\PostImageRepository;
use App\Repository\PostRequestRepository;
use App\Repository\SubcategoryRepository;
use App\Service\PermissionCheck;
use App\Service\PostManager;
use Doctrine\Common\Collections\ArrayCollection;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/request")
 */
class PostRequestController extends AbstractController
{

    private $postManager;

    public function __construct(PostManager $postManager)
    {
        $this->postManager = $postManager;
    }


    /**
     * @Route("/all", name="post_request_index", methods={"GET"})
     * @param PostRequestRepository $postRequestRepository
     * @param CategoryRepository $categoryRepository
     * @return Response
     */
    public function index(PostRequestRepository $postRequestRepository, CategoryRepository $categoryRepository): Response
    {
        return $this->render('post_request/index.html.twig', [
            'post_requests' => $postRequestRepository->findAll(),
            'categories' => $categoryRepository->findAll(),
            'name' => 'All'
        ]);
    }

    /**
     * @Route("/category/{categoryId}", name="post_request_category", methods={"GET"})
     * @param PostRequestRepository $postRequestRepository
     * @param CategoryRepository $categoryRepository
     * @param $categoryId
     * @return Response
     */
    public function categoryPosts(PostRequestRepository $postRequestRepository,
                                  CategoryRepository $categoryRepository, $categoryId): Response
    {

        $category = $categoryRepository->find($categoryId);

        return $this->render('post/index.html.twig', [
            'post_requests' => $postRequestRepository->findBy(['category' => $category]),
            'categories' => $categoryRepository->findAll(),
            'name' => $category->getName()
        ]);
    }

    /**
     * @Route("/subcategory/{subcategoryId}", name="post_request_subcategory", methods={"GET"})
     * @param PostRequestRepository $postRequestRepository
     * @param SubcategoryRepository $subcategoryRepository
     * @param CategoryRepository $categoryRepository
     * @param $subcategoryId
     * @return Response
     */
    public function subcategoryPosts(PostRequestRepository $postRequestRepository, SubcategoryRepository $subcategoryRepository,
                                     CategoryRepository $categoryRepository, $subcategoryId)
    {

        $subcategory = $subcategoryRepository->find($subcategoryId);

        return $this->render('post_request/index.html.twig', [
            'post_requests' => $postRequestRepository->findBy(['subcategory' => $subcategory]),
            'categories' => $categoryRepository->findAll(),
            'name' => $subcategory->getName()
        ]);
    }


    /**
     * @Route("/manage", name="post_request_manage", methods={"GET"})
     */
    public function manage(): Response
    {

        $user = $this->getUser();
        $userPostRequests = $user->getPostRequests();

        return $this->render('/post_request/manage.html.twig', [
            'post_requests' => $userPostRequests
        ]);
    }

    /**
     * @Route("/new", name="post_request_new", methods={"GET","POST"})
     * @param Request $request
     * @param CategoryRepository $categoryRepository
     * @param SubcategoryRepository $subcategoryRepository
     * @return Response
     */
    public function new(Request $request, CategoryRepository $categoryRepository, SubcategoryRepository $subcategoryRepository): Response
    {
        $postRequest = new PostRequest();

        $subcategories = $this->postManager->subcategoriesToArray();
        $categories = $this->postManager->categoriesToArray();

        $form = $this->createForm(PostRequestType::class, $postRequest);

        $postImagesCollection = new ArrayCollection();
        foreach ($postRequest->getPostImages() as $i) {
            $postImagesCollection->add($i);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $request->isXmlHttpRequest()) {

            $entityManager = $this->getDoctrine()->getManager();

            // get rid of the ones that the user got rid of in the interface (DOM)
            foreach ($postImagesCollection as $postImg) {
                if ($postRequest->getPostImages()->contains($postImg) === false) {
                    $entityManager->remove($postImg);
                }
            }

            $postRequest->setUser($this->getUser());
            $postRequest->setCategory($categoryRepository->find($request->get('post_request_category')));
            $postRequest->setSubcategory($subcategoryRepository->find($request->get('post_request_subcategory')));
            $postRequest->setCreatedAt(time());
            $entityManager->persist($postRequest);
            $entityManager->flush();

            return $this->redirectToRoute('post_request_index');
        }

        return $this->render('post_request/new.html.twig', [
            'post_request' => $postRequest,
            'form' => $form->createView(),
            'subcategoriesData' => $subcategories,
            'categoriesData' => $categories
        ]);
    }

    /**
     * @Route("/{id}", name="post_request_show", methods={"GET"})
     */
    public function show(PostRequest $postRequest): Response
    {
        return $this->render('post_request/show.html.twig', [
            'post_request' => $postRequest,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="post_request_edit", methods={"GET","POST"})
     * @param Request $request
     * @param PostRequest $postRequest
     * @param $id
     * @param PostImageRepository $postImageRepository
     * @param CategoryRepository $categoryRepository
     * @param SubcategoryRepository $subcategoryRepository
     * @return Response
     */
    public function edit(Request $request, PostRequest $postRequest, $id, PostImageRepository $postImageRepository,
                         CategoryRepository $categoryRepository, SubcategoryRepository $subcategoryRepository): Response
    {

        $this->denyAccessUnlessGranted('edit', $postRequest);

        $subcategories = $this->postManager->subcategoriesToArray();
        $categories = $this->postManager->categoriesToArray();
        $currentSubcategory = $postRequest->getSubcategory()->getId();
        $currentCategory = $postRequest->getCategory()->getId();

        $form = $this->createForm(PostRequestType::class, $postRequest);

        $postImagesCollection = new ArrayCollection();
        foreach ($postRequest->getPostImages() as $i) {
            $postImagesCollection->add($i);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $request->isXmlHttpRequest()) {


            $entityManager = $this->getDoctrine()->getManager();

            if (!empty($request->get('deleteImages'))) {

                foreach ($request->get('deleteImages') as $imgId) {

                    if ($imgId != 'undefined') {
                        $postImage = $postImageRepository->find($imgId);
                        $entityManager->remove($postImage);
                    }
                }

            }


            // get rid of the ones that the user got rid of in the interface (DOM)
            foreach ($postImagesCollection as $postImg) {
                if ($postRequest->getPostImages()->contains($postImg) === false) {
                    $postRequest->getPostImages()->removeElement($postImg);
                    $entityManager->remove($postImg);
                }
            }

            $postRequest->setCategory($categoryRepository->find($request->get('post_request_category')));
            $postRequest->setSubcategory($subcategoryRepository->find($request->get('post_request_subcategory')));

            $entityManager->persist($postRequest);
            $entityManager->flush();


            return $this->redirectToRoute('post_request_index', [
                'id' => $postRequest->getId(),
            ]);
        }

        return $this->render('post_request/edit.html.twig', [
            'post_request' => $postRequest,
            'form' => $form->createView(),
            'subcategoriesData' => $subcategories,
            'categoriesData' => $categories,
            'currentCategory' => $currentCategory,
            'currentSubcategory' => $currentSubcategory
        ]);
    }

    /**
     * @Route("/{id}/delete", name="post_request_delete", methods={"DELETE"})
     * @param Request $request
     * @param PostRequest $postRequest
     * @return Response
     */
    public function delete(Request $request, PostRequest $postRequest): Response
    {

        $this->denyAccessUnlessGranted('delete', $postRequest);

        if ($this->isCsrfTokenValid('delete' . $postRequest->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($postRequest);
            $entityManager->flush();
        }

        return $this->redirectToRoute('post_request_index');
    }
}
