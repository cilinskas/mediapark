<?php

namespace App\Service;


use App\Entity\Message;
use App\Repository\MessageRepository;
use Symfony\Component\Security\Core\Security;

class MessageManager
{

    private $security;
    private $messageRepository;

    public function __construct(Security $security, MessageRepository $messageRepository)
    {
        $this->security = $security;
        $this->messageRepository = $messageRepository;
    }


    public function getChatMessages(int $senderId) : array
    {
        $messagesReceived = $this->messageRepository->findMessagesBySenderAndRecipient($senderId, $this->security->getUser()->getId());
        $messagesSent = $this->messageRepository->findMessagesByRecipientAndSender($senderId, $this->security->getUser()->getId());

        $messages = array_merge(
            $messagesReceived,
            $messagesSent
        );

        usort($messages, array($this, "sortMessages"));

        return $messages;
    }

    public function sortMessages(Message $a, Message $b)
    {
        return strcmp($b->getSentAt(), $a->getSentAt());
    }

    public function getGroupedMessages() : array
    {
        // group messages which saved as being recipient
        $messages = $this->messageRepository->findMessagesByRecipient($this->security->getUser()->getId());
        $groupedMessages = [];
        foreach ($messages as $message) {
            if (!in_array($message->getSender(), $groupedMessages)) {
                array_push($groupedMessages, $message->getSender());
            }
        }

        // group messages which saved as being sender
        $messagesBySender = $this->messageRepository->findMessagesBySender($this->security->getUser()->getId());

        foreach ($messagesBySender as $m) {
            if (!in_array($m->getRecipient(), $groupedMessages)) {
                array_push($groupedMessages, $m->getRecipient());
            }
        }

        return $groupedMessages;

    }


}