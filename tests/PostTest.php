<?php

namespace App\Tests;


use App\Entity\Post;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PostTest extends WebTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    protected static $application;

    private $user;
    private $currency;
    private $category;
    private $subcategory;

    public function setUp()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        $this->em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $schemaTool = new SchemaTool($this->em);
        $metadata = $this->em->getMetadataFactory()->getAllMetadata();

        $schemaTool->dropSchema($metadata);
        $schemaTool->createSchema($metadata);

        /** @var Client $client */
        $client = static::createClient();
        $this->em = $client->getContainer()->get('doctrine.orm.entity_manager');

        $loader = new Loader();
        $loader->loadFromDirectory('src/DataFixtures/Tests');


        $purger = new ORMPurger();
        $executor = new ORMExecutor($this->em, $purger);
        $executor->execute($loader->getFixtures(), true);

        $this->user = $this->em->getRepository('App\Entity\User')->findOneBy(['id' => 1]);
        $this->currency = $this->em->getRepository('App\Entity\Currency')->findOneBy(['id' => 1]);
        $this->category = $this->em->getRepository('App\Entity\Category')->findOneBy(['id' => 1]);
        $this->subcategory = $this->em->getRepository('App\Entity\Subcategory')->findOneBy(['id' => 1]);

    }


    public function testAddPost()
    {

        $post = new Post();
        $post->setTitle('testTitle');
        $post->setPrice(199.99);
        $post->setCurrency($this->currency);
        $post->setDescription('Description Test');
        $post->setUser($this->user);
        $post->setSubcategory($this->subcategory);
        $post->setCategory($this->category);


        $this->em->persist($post);
        $this->em->flush();

        $post = $this->em->getRepository('App\Entity\Post')->findOneBy(['title' => 'testTitle']);

        $this->assertEquals('testTitle', $post->getTitle());

        $this->em->remove($post);
        $this->em->flush();

    }


    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();
        $this->em->close();
    }

}