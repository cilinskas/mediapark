<?php

namespace App\DataFixtures\Tests;

use App\Entity\Category;
use App\Entity\Currency;
use App\Entity\Post;
use App\Entity\PostRequest;
use App\Entity\Subcategory;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class TestFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {

        $faker = Factory::create();

        $categories = ['Graphics & Design', 'Digital Marketing', 'Writing & Translation', 'Video & Animation', 'Music & Audio', 'Programming & Tech', 'Business', 'Lifestyle'];
        $subcategories = ['Logo Design', 'Banner Ads', 'SEO', 'Social Media Marketing', 'Sales Copy', 'Translation', 'Short Video Ads', 'Animation GIFs', 'Voice Over', 'Mixing & Mastering',
            'Mobile Apps & Web', 'Ecommerce', 'Market Research', 'Business Plans', 'Health, Nutrition & Fitness', 'Online Lessons'];
        $currencies = ['EUR', 'USD', 'GBP'];

        $categoryObjectsArray = [];
        $subcategoryObjectsArray = [];
        $currencyObjectsArray = [];

        $index = 0;
        foreach ($categories as $categoryName) {
            $category = new Category();
            $category->setName($categoryName);
            $category->setCreatedAt(time());
            $manager->persist($category);
            $categoryObjectsArray[] = $category;

            for ($i = 0; $i < 2; $i++) {
                $subcategory = new Subcategory();
                $subcategory->setName($subcategories[$index]);
                $subcategory->setCategory($category);
                $subcategory->setCreatedAt(time());
                $index++;
                $manager->persist($subcategory);
                $subcategoryObjectsArray[] = $subcategory;
            }
        }

        foreach ($currencies as $currencyName) {
            $currency = new Currency();
            $currency->setName($currencyName);
            $currency->setCreatedAt(time());
            $manager->persist($currency);
            $currencyObjectsArray[] = $currency;
        }


        //business user

        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setFirstName($faker->firstName);
            $user->setLastName($faker->lastName);
            $user->setBusinessName($faker->userName);
            $user->setAddress($faker->address);
            $user->setCity($faker->city);
            $user->setEmail($faker->email);
            $user->setRoles(['ROLE_BUSINESS']);
            //set password = 'password'
            $user->setPassword('$argon2i$v=19$m=1024,t=2,p=2$S2lxeDI1M1VUZy5XUkhDWA$sU6pTDDwxWnj2UtFRFPvShHwPlIgkiVaq82gpqcqCxk');
            $user->setCreatedAt(time());
            $manager->persist($user);

            //posts

            // categories ID range 1-8
            $categoryIndex = $faker->numberBetween(0, 7);
            //subcategories ID range 1-16
            //each category has 2 subcategories
            $subcategoryIndex = $categoryIndex * 2;

            $category = $categoryObjectsArray[$categoryIndex];
            $subcategory = $subcategoryObjectsArray[$subcategoryIndex];
            $currency = $currencyObjectsArray[$faker->numberBetween(0, 2)];


            $post = new Post();
            $post->setUser($user);
            $post->setCategory($category);
            $post->setSubcategory($subcategory);
            $post->setCurrency($currency);
            $post->setDeliveryTimeFrom($faker->numberBetween(1, 15));
            $post->setDeliveryTimeTo($faker->numberBetween(15, 90));
            $post->setDeliveryTimeFormat('days');
            $post->setTitle($faker->jobTitle);
            $post->setDescription($faker->text);
            $post->setPrice($faker->numberBetween(10, 5000));
            $post->setCreatedAt(time());

            $manager->persist($post);

        }

        //client user

        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setFirstName($faker->firstName);
            $user->setLastName($faker->lastName);
            $user->setAddress($faker->address);
            $user->setCity($faker->city);
            $user->setEmail($faker->email);
            $user->setRoles(['ROLE_CLIENT']);
            //set password = 'password'
            $user->setPassword('$argon2i$v=19$m=1024,t=2,p=2$S2lxeDI1M1VUZy5XUkhDWA$sU6pTDDwxWnj2UtFRFPvShHwPlIgkiVaq82gpqcqCxk');
            $user->setCreatedAt(time());
            $manager->persist($user);

            //post Requests

            // categories ID range 1-8
            $categoryIndex = $faker->numberBetween(0, 7);
            //subcategories ID range 1-16
            //each category has 2 subcategories
            $subcategoryIndex = $categoryIndex * 2;


            $category = $categoryObjectsArray[$categoryIndex];
            $subcategory = $subcategoryObjectsArray[$subcategoryIndex];
            $currency = $currencyObjectsArray[$faker->numberBetween(0, 2)];

            $post = new PostRequest();
            $post->setUser($user);
            $post->setCategory($category);
            $post->setSubcategory($subcategory);
            $post->setCurrency($currency);
            $post->setDeliveryTimeFrom($faker->numberBetween(1, 15));
            $post->setDeliveryTimeTo($faker->numberBetween(15, 90));
            $post->setDeliveryTimeFormat('days');
            $post->setTitle($faker->jobTitle);
            $post->setDescription($faker->text);
            $post->setPrice($faker->numberBetween(10, 5000));
            $post->setCreatedAt(time());

            $manager->persist($post);

        }


        $manager->flush();
    }



}